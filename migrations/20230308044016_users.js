exports.up = async function(knex) {
    if (! (await knex.schema.hasTable('users')) ) {
        await knex.schema
          .createTableIfNotExists("users", function (table) {
            table.increments("id");
            table.string("nama", 255).notNullable();
            table.string("email").notNullable();
            table.string("address");
            table.string("phone");
            table.timestamp("created_at");
            table.string("created_by");
            table.timestamp("updated_at");
            table.timestamp("deleted_at");
            table.string("updated_by");
          })
      }    
  };
  
  exports.down = function (knex, Promise) {
    return knex.schema.dropTableIfExists("users");
  };
  