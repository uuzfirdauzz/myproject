var express = require('express');
var router = express.Router();

const usersMenu = require('./users');

router.post('/users', usersMenu.create)
router.post('/users/all', usersMenu.all)
router.get('/users/:id', usersMenu.edit)
router.put('/users/:id', usersMenu.update)
router.delete('/users/:id', usersMenu.deleted)

module.exports = router;

