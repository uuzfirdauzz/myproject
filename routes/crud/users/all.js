const knex = require("../../../config/knex");
const { request } = require('express');
const { attachPaginate } = require('knex-paginate');
attachPaginate();

module.exports = async function (req, res) {
    try {
        const request = req.body;

        /** Filter */
        const data = await knex("users").modify(async (query) => {
            if (request.nama !== undefined && request.nama != null) {
                query.whereRaw(`(LOWER(users.nama) like '%${(request.nama).toString().toLowerCase()}%' OR LOWER(users.email) like '%${(request.nama).toString().toLowerCase()}%' OR LOWER(users.address) like '%${(request.nama).toString().toLowerCase()}%')`)
            }
            if (request.phone !== undefined && request.phone != null) {
                query.whereRaw(`(LOWER(users.phone) like '%${(request.phone).toString().toLowerCase()}%')`)
            }
        })
            .paginate({
                perPage: request.perPage,
                currentPage: request.currentPage,
            });

        return res.json({
            status: "success",
            data: data.data,
            pagination: data.pagination,
            message: "data successfully loaded",
        });

    } catch (error) {
        console.log(error);
        return res.status(500).json({ status: "error", message: error });
    }
};
