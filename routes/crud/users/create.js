const validator = require('fastest-validator');
const v = new validator();
const knex = require("../../../config/knex");
const { request } = require('express');

module.exports = async function (req, res) {
  try {
    const request = req.body;

    /** validate users input */
    const schema = {
      nama: { type: "string", empty: false },
      email: { type: "email", empty: false },
      address: { type: "string", string: false },
      phone: { type: "string", optional: true },
    };
    const validate = v.validate(request, schema);
    if (validate.length) {
      return res.status(400).json({
        status: "error",
        message: validate
      });
    }

    const data = {
      nama: request.nama,
      email: request.email,
      address: request.address,
      phone: request.phone,
      created_at: new Date(),
      created_by: "Admin",
    };

    /** Query insert user */
    const insertData = await knex("users").insert(data).returning("*");

    return res.status(200).json({
      status: "success",
      data: insertData,
      message: "data successfully saved",
    });

  } catch (error) {
    console.log(error);
    return res.status(500).json({ status: "error", message: error });
  }
};
