const knex = require("../../../config/knex");

module.exports = async function (req, res) {
  try {
    const id = req.params.id;

    const existUsers = await knex("users").where("id", id).select("id");

    if (existUsers.length <= 0) throw "user doesn't exist";

    await knex("users").where("id", id).del();

    return res.status(200).json({
      status: "success",
      message: "data successfully deleted",
    });
  } catch (error) {
    return res.status(500).json({ status: "error", message: error });
  }
};
