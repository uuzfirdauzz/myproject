const knex = require("../../../config/knex");

module.exports = async function (req, res) {
    try {
        const id = req.params.id;
        const data = await knex("users")
            .select("*")
            .where("id", id)
            .first();

        return res.status(200).json({
            status: "success",
            data: data,
            message: "data successfully saved",
        });
    } catch (error) {
        return res.status(500).json({ status: "error", message: error });
    }
};
