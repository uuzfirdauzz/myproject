const all = require("./all");
const create = require("./create");
const deleted = require("./destroy");
const update = require("./update");
const edit = require("./edit");

module.exports = {
    all,
    create,
    deleted,
    update,
    edit,
};