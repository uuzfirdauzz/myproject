const validator = require('fastest-validator');
const v = new validator();
const knex = require("../../../config/knex");

module.exports = async function (req, res) {
    try {
        const request = req.body;
        const id = req.params.id;

        /** validate users input */
        const schema = {
            nama: { type: "string", empty: false },
            email: { type: "email", empty: false },
            address: { type: "string", empty: false },
        };
        const validate = v.validate(request, schema);
        if (validate.length) {
            return res.status(400).json({
                status: "error",
                message: validate
            });
        }

        /** check is email sudah terdaftar atau belum */
        var iExist = await knex("users").where("email", request.email).first();
        if (iExist && iExist.id != id) {
            return res.status(404).json({
                status: "error",
                message: `User with email ${request.email} already exists`,
            });
        }

        /** Check data ada atau tidak */
        iExist = await knex("users").where("id", id).first();
        if (!iExist) {
            return res.status(404).json({
                status: "error",
                message: "data not found",
            });
        }

        const data = {
            nama: request.nama,
            email: request.email,
            address: request.address,
            phone: request.phone,
            updated_at: new Date(),
            updated_by: "Admin",
        };

        /** Query data user */
        await knex("users").where("id", id).update(data);

        return res.status(200).json({
            status: "success",
            data: {
                id: id
            },
            message: "data successfully saved",
        });
    } catch (error) {
        console.log(error)
        return res.status(500).json({ status: "error", message: error });
    }
};
